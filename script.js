/**
 * @module
 */
const Incrementor = (newCard) => {

    let increment = 1;
    let countValue = 0;
    let card, cardButton, cardCounter;


        card = newCard;

        cardButton = card.querySelector('[aria-label="increment-clicker"]');
        cardButton.addEventListener("click", count, false);


    function count() {
        countValue += increment;
        updateCard(countValue);
        return countValue;
    }

    /**extra function to split CSS fiddle from calculation*/
    function updateCard(newValue) {
        console.log(newValue);
        cardCounter = card.querySelector('.counter');
        cardCounter.setAttribute('value',newValue);
    }

    function setIncrement(value) {
        increment = value;
    }

    return {
        setIncrement: setIncrement
    };

};

let elements = [];
let cards = document.getElementsByClassName('increment-card');
for(var i = 0; i < cards.length; i++) {
    console.log(cards[i]);
    elements[i] = Incrementor(cards[i]);
    elements[i].setIncrement(i+1);
}
console.log(elements);