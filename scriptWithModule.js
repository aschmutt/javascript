/**
 * @module
 */
const Module = (() => {

    function Incrementor(card) {

        this.increment = 1;
        this.countValue = 0;
        this.cardButton;
        this.cardCounter;

        cardButton = card.querySelector('[aria-label="increment-clicker"]');
        cardButton.addEventListener("click", count, false);

        function count() {
            countValue += increment;
            updateCard(countValue);
        }

        /**extra function to split CSS fiddle from calculation*/
        function updateCard(newValue) {
            console.log(newValue);
            cardCounter = card.querySelector('.counter');
            console.log(cardCounter);
            cardCounter.setAttribute('value', newValue);
        }

        function setIncrement(value) {
            increment = value;
        }

        return {
            setIncrement:setIncrement
        }

    }

    return {
        Incrementor: Incrementor
    };

})();

let elements = [];
let cards = document.getElementsByClassName('increment-card');
for(var i = 0; i < cards.length; i++) {
    console.log(cards[i]);
    elements[i] = new Module.Incrementor(cards[i]);
    //elements[i].setIncrement(i);
}
console.log(elements);