(function(window, document){
    let localVar = 'Ich bin lokal!';

    function myLocalTest(){
        let output = '';
        if (typeof localVar !== 'undefined') {
            output += 'Test Lokal: ' + localVar + '</br>';
        }

        output += 'Test Lokal: ' + globalVar;
        document.getElementById('display2').innerHTML = output;

    }

    let el = document.getElementById("clicker");
    if (el.addEventListener) {
        el.addEventListener("click", myLocalTest);
    }

    else if (el.attachEvent) {
        el.attachEvent('onclick', myLocalTest);
    }

    window.myLocalTest = myLocalTest;
})(window, document);

let globalVar = 'ich bin global!';

let clicker = document.getElementById('clicker');
clicker.addEventListener("click", listener, false);

function listener() {
    output = myGlobalTest();
    document.getElementById('display').innerHTML = output;
}

function myGlobalTest() {
    let output = '';
    if (typeof localVar !== 'undefined') {
        output += 'Test Global: ' + localVar + '</br>';
    }

    output += 'Test Global: ' + globalVar;
    return output;
}
